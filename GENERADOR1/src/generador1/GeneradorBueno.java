package generador1;

import java.util.Scanner;

public class GeneradorBueno {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		mostrarMenu();
		
		int longitud = scanner.nextInt();
		
		int opcion = eleccionOpcion(scanner);
		
		String password = "";
		
		password = menu(longitud, opcion, password);

		System.out.println(password);
		scanner.close();
	}

	private static String menu(int longitud, int opcion, String password) {

		switch (opcion) {
		case 1:
			password = caracteresAz(longitud, password);
			break;
		case 2:
			password = numerosOpcion2(longitud, password);
			break;
		case 3:
			password = numerosCaracteresEspeciales(longitud, password);
			break;
		case 4:
			password = letrasNumerosCaracteresEspeciales(longitud, password);
			break;
		}
		return password;
	}

	private static String letrasNumerosCaracteresEspeciales(int longitud, String password) {
		char letra4;
		char caracter4;
		int numero4;
		int n;
		for (int i = 0; i < longitud; i++) {
			n= (int) (Math.random() * 3);
				if (n == 1) {
				
					letra4 = (char) ((Math.random() * 26) + 65);
					password += letra4;
				} else if (n == 2) {
					caracter4= (char) ((Math.random() * 15) + 33);
					password += caracter4;
				} else {
					numero4=(int) (Math.random() * 10);
					password += numero4;
				}
		}
		return password;
	}

	private static String numerosCaracteresEspeciales(int longitud, String password) {
		int n;
		char letra3;
		char caracter3;
		for (int i = 0; i < longitud; i++) {
			
				n = (int) (Math.random() * 2);
				if (n == 1) {
					letra3=(char) ((Math.random() * 26) + 65);
					password += letra3;
				} else {
					caracter3=(char) ((Math.random() * 15) + 33);
					password += caracter3;
				}
		}
		return password;
	}

	private static String numerosOpcion2(int longitud, String password) {
		int numero2;
		for (int i = 0; i < longitud; i++) {
			numero2=(int) (Math.random() * 10);
			password += numero2;
		}
		return password;
	}

	private static String caracteresAz(int longitud, String password) {
		char letra1;
		for (int i = 0; i < longitud; i++) {
			letra1=(char) ((Math.random() * 26) + 65);
			password += letra1;
		}
		return password;
	}

	private static int eleccionOpcion(Scanner scanner) {
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		return opcion;
	}

	private static void mostrarMenu() {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		System.out.println("Introduce la longitud de la cadena: ");
	}

}
	
