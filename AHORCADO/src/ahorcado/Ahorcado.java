package ahorcado;
import java.io.File;
import java.util.Scanner;
public class Ahorcado {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private final static String RUTA = "src\\Ahorcado\\palabras.txt";
	private static String[] palabras= new String[NUM_PALABRAS];
	
	public static void main(String[] args) {
				
				String caracteresElegidos = "";
				
				leeFichero();

				Scanner input = new Scanner(System.in);

				String palabraSecreta = palabraAleatoria();

				char[][] caracteresPalabra = creacionArray(palabraSecreta);
			
				System.out.println("Acierta la palabra");
				
				caracteresElegidos = juego(palabraSecreta, input, caracteresPalabra, caracteresElegidos);

				input.close();
			}

	private static char[][] creacionArray(String palabraSecreta) {
		char[][] caracteresPalabra = new char[2][];
		
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		return caracteresPalabra;
	}

	private static String palabraAleatoria() {
		String palabraSecreta;
		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
		return palabraSecreta;
	}

	private static String juego(String palabraSecreta, Scanner input, char[][] caracteresPalabra,
			String caracteresElegidos) {
		int fallos;
		boolean acertado;
		do {

			System.out.println("####################################");

			mostrarCaracteres(caracteresPalabra);

			System.out.println("Introduce una letra o acierta la palabra");
			
			System.out.println("Caracteres Elegidos: " + caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();
			fallos = 0;

			
			
			fallos = contadorFallos(caracteresPalabra, caracteresElegidos, fallos);

			mostrarAhorcado(fallos);

			juegoPerdido(palabraSecreta, fallos);
			
			acertado = comprobacionAcierto(caracteresPalabra);
			
			jugadaAcertada(acertado);

		} while (!acertado && fallos < FALLOS);
		return caracteresElegidos;
	}

	private static boolean comprobacionAcierto(char[][] caracteresPalabra) {
		boolean acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		return acertado;
	}

	private static int contadorFallos(char[][] caracteresPalabra, String caracteresElegidos, int fallos) {
		boolean encontrado;
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
		return fallos;
	}

	private static void mostrarCaracteres(char[][] caracteresPalabra) {
		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();
	}

	private static void jugadaAcertada(boolean acertado) {
		if (acertado)
			System.out.println("Has Acertado ");
	}

	private static void juegoPerdido(String palabraSecreta, int fallos) {
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
	}

	private static void leeFichero() {

		File fich = new File(RUTA);
		Scanner inputFichero = null;

		try {
			inputFichero = new Scanner(fich);
			
			for (int i = 0; i < NUM_PALABRAS; i++) {
				
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			
			
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			
			if (fich != null && inputFichero != null) {
				inputFichero.close();
			}
		}
	}

	private static void mostrarAhorcado(int fallos) {
		switch (fallos) {
			case 1:
	
				System.out.println("     ___");
				break;
			case 2:
	
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 3:
				System.out.println("  ____ ");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 4:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 5:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 6:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println(" T    |");
				System.out.println("     ___");
				break;
			case 7:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println(" T    |");
				System.out.println(" A   ___");
				break;
		}
	}

		
	}


