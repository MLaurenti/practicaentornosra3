package refactorizar2;

import java.util.Scanner;

public class Refactorizar2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Elimino variable static scanner y la pongo en el metodo main
		//Cambio de nombre de la variable Scanner a lector
		 Scanner lector = new Scanner(System.in);
		//Variable cantidad maxima de alumnos eliminada porque no es usada
		int arrays[] = new int[10];
		
		//Elimino la variable n y la creo directamente en el bucle for
		//Separar con espacios las operacion del bucle for para que el codigo sea m�s legible
		for(int n = 0 ; n < 10 ; n++)
		{
			System.out.println("Introduce nota media de alumno");
			arrays[n] = lector.nextInt();
		}	
		
		System.out.println("El resultado es: " + recorrer_array(arrays));
		
		lector.close();
	}
	static double recorrer_array(int vector[])
	{
		//El nombre de las variables tiene que ser mas apropiado
		double suma = 0;
		for(int a=0;a<10;a++) 
		{
			suma=suma+vector[a];
		}
		return suma/10;
	}
	
}